# PSO2-Keeper
The totally renew of PSO2-Keeper, it will be huge size than the origin one.    
https://www.pso2keeper.com/       
![image1](https://i.imgur.com/Gsf9GU4.png)

![image2](https://i.ibb.co/ZWnq0Gw/123.png)

## File Tree

    ├─public
    │  └─img
    │      └─icons
    ├─src
    │  ├─assets
    │  ├─components
    │  │  ├─bar
    │  │  │  ├─alertBar
    │  │  │  ├─appsBar
    │  │  │  ├─controlBar
    │  │  │  └─snackBar
    │  │  ├─card
    │  │  │  ├─groupCard
    │  │  │  ├─joinCard
    │  │  │  └─loginCard
    │  │  └─dialog
    │  │      ├─editDialog
    │  │      ├─joinDialog
    │  │      ├─permissionDialog
    │  │      └─selectMissionDialog
    │  ├─plugins
    │  ├─router
    │  ├─store
    │  └─views

## Tech Using

    1. Vuejs 2.6
    2. Vuetify 2.1
    3. Vuex
    4. Vue-Router
    5. Vue-Draggab
    6. LineBot
    7. Firebase


## Set up

    1. npm i --save
    2. npm run serve