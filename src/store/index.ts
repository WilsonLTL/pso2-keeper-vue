import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const alertBar = {
  state: {
    status: false,
    title: '',
    group_key: '',
    permission_key: ''
  },
  mutations: {
    setAlertBarState (state:any, payload:any) {
      state.status = payload.status
      state.title = payload.title
      state.group_key = payload.group_key
      state.permission_key = payload.permission_key
    }
  }
}
const app = {
  state: {
    login_status: false,
    saveBtnStatus: false,
    size: 0
  },
  mutations: {
    setLoginState (state:any, payload:boolean) {
      state.login_status = payload
    },
    setSaveBtnStatus (state:any, payload:boolean) {
      state.saveBtnStatus = payload
    },
    setSize (state:any, payload:number) {
      state.size = payload
    }
  }
}
const appsBar = {
  state: {
    loading_status: false,
    title: 'PSO2-Keeper'
  },
  mutations: {
    setLoadingStatus (state:any, payload: boolean) {
      state.loading_status = payload
    }
  }
}
const controlBar = {
  state: {
    edit_status: false,
    tags: [
      {
        status: true,
        value: '隊長',
        icon: 'mdi-label'
      },
      {
        status: true,
        value: 'TE',
        icon: 'mdi-christianity'
      },
      {
        status: true,
        value: 'RA',
        icon: 'mdi-pistol'
      },
      {
        status: true,
        value: '推送訊息',
        icon: 'mdi-message'
      }
    ]
  },
  mutations: {
    setControlBarEditStatus (state:any, payload:boolean) {
      state.edit_status = payload
    }
  }
}
const editDialog = {
  state: {
    item: {},
    status: false,
    classes: ['BO', 'BR', 'ET', 'FI', 'FO', 'GU', 'HR', 'HU', 'PH', 'RA', 'SU', 'TE']
  },
  mutations: {
    setEditDialog (state:any, payload:any) {
      state.status = payload.status
      state.item = payload.item
    }
  }
}
const groupCard = {
  state: {
    list: [],
    unform: [],
    group_player: {}
  },
  mutations: {
    setGroupCardState (state:any, payload:any) {
      state.list = payload.list
      state.unform = payload.unform
      state.group_player = payload.group_player
    }
  }
}
const joinCard = {
  state: {
    groupKey: '',
    permissionToken: '',
    name: '',
    quota: 0,
    img: '',
    players: []
  },
  mutations: {
    setMission (state:any, payload:any) {
      state.groupKey = payload.groupKey
      state.permissionToken = payload.permissionToken
      state.name = payload.name
      state.quota = payload.quota
      state.img = payload.img
      state.players = payload.players
    },
    addNewPlayer (state:any, payload:JSON) {
      state.players.push(payload)
    }
  }
}
const joinDialog = {
  state: {
    name: '',
    class: '',
    classes: ['BO', 'BR', 'ET', 'FI', 'FO', 'GU', 'HR', 'HU', 'PH', 'RA', 'SU', 'TE'],
    status: false
  },
  mutations: {
    setJoinDialogStatus (state:any, payload:boolean) {
      state.status = payload
    }
  }
}
const loginCard = {
  state: {
    key: ''
  }
}
const permissionDialog = {
  state: {
    status: false,
    permission_key: ''
  },
  mutations: {
    setPermissionDialogStatus (state:any, payload:boolean) {
      state.status = payload
    }
  }
}
const pushMessageDialog = {
  state: {
    custom_message: '',
    default_message: false,
    status: false
  },
  mutations: {
    setPushMessageDialogStatus (state:any, payload:any) {
      state.status = payload
    }
  }
}
const selectMissionDialog = {
  state: {
    img: 'img/pso2_wallpaper1.jpg',
    quota: 0,
    quotas: [],
    permission_code: '',
    mission: '',
    mission_name_list: ['第6使徒殲滅作戦'],
    missions: [
      {
        img: 'https://p2.bahamut.com.tw/M/2KU/87/3865a9d1a42034a026f23d14b315hl35.JPG',
        name: '第6使徒殲滅作戦',
        quota: 8
      }
    ],
    status: false
  },
  mutations: {
    setSelectMissionDialogStatus (state:any, payload:boolean) {
      state.status = payload
    },
    setSelectMissionDialogMissions (state:any, payload:Array<JSON>) {
      state.missions = payload
      state.mission_name_list = []
      state.missions.forEach((item:any) => {
        state.mission_name_list.push(item.name)
      })
    }
  }
}
const snackBar = {
  state: {
    icon: '',
    color: 'primary',
    status: false,
    text: 'hi',
    timeout: 2000
  },
  mutations: {
    setSnackBar (state:any, payload: any) {
      state.icon = payload.icon
      state.status = payload.status
      state.color = payload.color
      state.text = payload.text
    }
  }
}

export default new Vuex.Store({
  modules: {
    alertBar: alertBar,
    app: app,
    appsBar: appsBar,
    controlBar: controlBar,
    editDialog: editDialog,
    groupCard: groupCard,
    joinCard: joinCard,
    joinDialog: joinDialog,
    loginCard: loginCard,
    permissionDialog: permissionDialog,
    pushMessageDialog: pushMessageDialog,
    selectMissionDialog: selectMissionDialog,
    snackBar: snackBar
  }
})
